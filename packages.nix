{ config, pkgs, ...}:

{
  home.packages = with pkgs; [
    # GPG
    pinentry-qt # pinentry-gtk2 
    # PASSWORDS
    pwgen mkpasswd
    # GIT
    nix-prefetch-git nix-prefetch-github 
    git-crypt gitAndTools.hub gitAndTools.lab
    # LISP
    sbcl rlwrap
    # VIM
    python38Packages.pylint ctags nixfmt rustfmt
    # MAIL
    xapian notmuch-addrlookup 
    # media edition
    imagemagick pdftk peek dia inkscape gimp libreoffice musescore krita
    vlc scrot byzanz kdenlive xournal youtube-dl zim
    # ERGODOX
    wally-cli
    # SHELL TOOLS
    moc pastebinit nethogs xorg.xev xorg.xmodmap tree ack silver-searcher curl
    mc unzip wget lm_sensors file zip rsync go-mtpfs sysfsutils ext4magic
    p7zip unrar jmtpfs fsarchiver compsize tcpdump cifs-utils xinput_calibrator
    # GRAPHICAL TOOLS
    peek xfce.thunar nicotine-plus transmission-gtk onboard pavucontrol vlc
    # CHAT
    weechat tdesktop element-desktop
    # BROWSERS
    next chromium
    # RUST
    # cargo rustc rustup
    ####
     gcc-unwrapped gcc gnumake i3lock-fancy terminator polkit_gnome
  ];
}
