set nocompatible
set title

set encoding=utf-8
set fileencoding=utf-8

set number
set nowrap
set mouse=a
let mapleader=","
let maplocalleader=";"
set bs=2

syntax enable
let python_highlight_all = 1

set termguicolors
set background=dark
colorscheme selenized

set incsearch
set hlsearch
set ignorecase
set smartcase

match ErrorMsg /\s\+$\| \+\ze\t/

set expandtab           " enter spaces when tab is pressed
set tabstop=4           " use 4 spaces to represent tab
set softtabstop=4
set shiftwidth=4        " number of spaces to use for auto indent
set showcmd                     " show (partial) command in status line

set colorcolumn=80
set cul
set fileformat=unix
set hid

set noswapfile

set splitbelow
set splitright

let g:ackprg = 'ag --vimgrep'
let g:ack_autoclose = 1

set laststatus=2
let g:airline_theme='luna'
let g:airline_symbols = {}
let g:airline_powerline_fonts = 1
let g:airline#extensions#ale#enabled = 1

let g:ale_linters = {
\    'python': ['pylint'],
\    'cpp': ['cppcheck', 'clang'],
\    'rust': ['cargo', 'rustc'],
\    'latex': ['chktex'],
\}
let g:ale_sign_error = '✗'
let g:ale_sign_warning = '⚠'

let g:airline#extensions#ale#error_symbol = '✗ '
let g:airline#extensions#ale#warning_symbol = '⚠ '

let g:ale_python_pylint_options = '--rcfile /home/matthieu/.vim/pylintrc'

set rtp+=~/.fzf

let g:fzf_colors =
\ { 'fg':      ['fg', 'Normal'],
\ 'bg':      ['bg', 'Normal'],
\ 'hl':      ['fg', 'Comment'],
\ 'fg+':     ['fg', 'CursorLine', 'CursorColumn', 'Normal'],
\ 'bg+':     ['bg', 'CursorLine', 'CursorColumn'],
\ 'hl+':     ['fg', 'Statement'],
\ 'info':    ['fg', 'PreProc'],
\ 'border':  ['fg', 'Ignore'],
\ 'prompt':  ['fg', 'Conditional'],
\ 'pointer': ['fg', 'Exception'],
\ 'marker':  ['fg', 'Keyword'],
\ 'spinner': ['fg', 'Label'],
\ 'header':  ['fg', 'Comment'] }

autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
let g:NERDTreeDirArrowExpandable = '▸'
let g:NERDTreeDirArrowCollapsible = '▾'
let NERDTreeIgnore=['\~$', '\.pyc$', '\.class$', '\.hi$', '\.o$', '\.aux$', '\.nav$', '\.log$', '\.out$', '\.pdf$', '\.snm$', '\.toc$', '\.sty$']    " Files to ignore (default = ['\~$'])
let NERDTreeQuitOnOpen=1
let NERDTreeMinimalUI=1
let NERDTreeAutoDeleteBuffer = 1

nnoremap <silent><expr> <F4> g:NERDTree.IsOpen() ? ":NERDTreeClose\<CR>" : bufname("%") == "" ? ":NERDTreeCWD\<CR>" : ":NERDTreeFind\<CR>"
nnoremap <silent> <F5> :TagbarToggle <CR> \| :TagbarTogglePause<CR>
nnoremap <silent> <F6> :UndotreeToggle<cr>
nnoremap <silent> <F12> :nohl<CR>
nnoremap <C-t> :tabnew<CR>


"désactivation de la surbrillance de la dernière recherche ctrl + n
nnoremap <Leader>* :nohl<CR>

"classer avec leader + s
vnoremap <Leader>s :sort<CR>

"manipuler des blocs complets avec < et >
vnoremap < <gv
vnoremap > >gv

nnoremap <Leader>k :CloseHiddenBuffers<CR>

nnoremap <Leader>- yyp<c-v>$r-
nnoremap <Leader>+ yyp<c-v>$r+
nnoremap <Leader>_ yyp<c-v>$r_
nnoremap <Leader>= yyp<c-v>$r=

"split navigations
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>


nmap <silent> <unique> + <Plug>nextvalInc
nmap <silent> <unique> - <Plug>nextvalDec

let g:tagbar_autofocus = 1
let g:tagbar_autoclose = 1
let g:tagbar_sort = 0
let g:tagbar_show_linenumbers = 1
let g:tagbar_compact = 1

if !exists('g:undotree_WindowLayout')
  let g:undotree_WindowLayout = 3
endif

"" you complete me
let g:ycm_auto_trigger = 0
