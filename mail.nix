{ config, pkgs, ...}:


let
  fwzte_matthieu = "fwzte_matthieu";
  fwzte_ml = "fwzte_ml";
  hawa_kiwi_matthieu = "hawa_kiwi_matthieu";
  gmail_meq_matt_astroid = "gmail_meq_matt_astroid";
  fwzte_matthieu_address = import ./secrets/malignly.nix;
  fwzte_ml_address = import ./secrets/overthought.nix;
  gmail_matthieu_address = import ./secrets/explementary.nix;
  hawa_kiwi_matthieu_address = import ./secrets/embark.nix;
  fwzte_git_address = import ./secrets/appositionally.nix;
  gpg_key = import ./secrets/indented.nix;
  phone = import ./secrets/conceder.nix;
  imap_gandi = {
    host = "mail.gandi.net";
    port = 993;
    tls.enable = true;
  };
  smtp_gandi = {
    host = "mail.gandi.net";
    port = 465;
    tls.enable = true;
  };
  imap_gmail = {
    host = "imap.googlemail.com";
    port = 993;
    tls.enable = true;
  };
  smtp_gmail = {
    host = "smtp.googlemail.com";
    port = 465;
    tls.enable = true;
  };
  realName = "Matthieu MÉQUIGNON";
  publicSignatureText = ''
    ${realName}
  '';
  privateSignatureText = ''
    ${publicSignatureText}
    ${phone}
  '';
in {

  programs.afew = {
    enable = true;
    extraConfig = builtins.readFile secrets/galenobismutite.nix;
  };

  programs.notmuch = {
    enable = true;
    hooks = {
      preNew = "mbsync --all";
      postNew = "afew --tag --new";
    };
    new = {
      tags = ["new"];
    };
  };
  programs.astroid = {
    enable = true;
    pollScript = ''
      notmuch new
    '';
    externalEditor = "terminator -x \"vim -c 'set ft=mail' '+set fileencoding=utf-8' '+set ff=unix' '+set enc=utf-8' '+set fo+=w' %1\"";
    extraConfig = {
      thread_view = {
        indent_messages = "true";
        default_save_directory = "~/Downloads/astroid/";
      };
      thread_index = {
        cell = {
          date_length = "15";
          message_count_length = "6";
          authors_length = "60";
          hidden_tags = "attachment,flagged,unread,matthieu_fwzte,matthieu_hawa_kiwi,signed,sent,inbox";
        };
      };
      mail = {
        close_on_success = true;
        # reply : TODO
      };
      poll.interval = 900;
      editor.charset = "utf-8";
      startup.queries = {
        "inbox" = "tag:inbox";
        "gmail" = "tag:matthieu_gmail";
      };
    };
  };
  programs.neomutt = {
    enable = true;
    editor = "vim";
    vimKeys = true;
    sidebar = {
      enable = true;
      shortPath = true;
    };
  };
  programs.mbsync.enable = true;
  programs.msmtp.enable = true;

  accounts.email = {
    accounts = {
      "${fwzte_matthieu}" = {
        primary = true;
        address = "${fwzte_matthieu_address}";
        userName = "${fwzte_matthieu_address}";
        passwordCommand = "pass mail/${fwzte_matthieu} | head -n 1";
        imap = imap_gandi;
        smtp = smtp_gandi;
        realName = "${realName}";
        astroid = {
          enable = true;
        };
        neomutt = {
          enable = true;
        };
        notmuch.enable = true;
        mbsync = {
          enable = true;
          create = "maildir";
        };
        msmtp.enable = true;
        signature = {
          text = "${privateSignatureText}";
          showSignature = "append";
        };
        gpg = {
          key = "${gpg_key}";
        };
      };
      "${hawa_kiwi_matthieu}" = {
        primary = false;
        address = "${hawa_kiwi_matthieu_address}";
        userName = "${hawa_kiwi_matthieu_address}";
        passwordCommand = "pass mail/${hawa_kiwi_matthieu} | head -n 1";
        imap = imap_gandi;
        smtp = smtp_gandi;
        realName = "Matthieu Méquignon";
        astroid = {
          enable = true;
        };
        neomutt = {
          enable = true;
        };
        notmuch.enable = true;
        mbsync = {
          enable = true;
          create = "maildir";
        };
        msmtp.enable = true;
        signature = {
          text = "${privateSignatureText}";
          showSignature = "append";
        };
        gpg = {
          key = "${gpg_key}";
        };
      };
      "${fwzte_ml}" = {
        primary = false;
        address = "${fwzte_ml_address}";
        userName = "${fwzte_ml_address}";
        passwordCommand = "pass mail/${fwzte_ml} | head -n 1";
        imap = imap_gandi;
        smtp = smtp_gandi;
        realName = "Matthieu Méquignon";
        astroid = {
          enable = true;
        };
        neomutt = {
          enable = true;
        };
        notmuch.enable = true;
        mbsync = {
          enable = true;
          create = "maildir";
        };
        msmtp.enable = true;
        signature = {
          text = "${publicSignatureText}";
          showSignature = "append";
        };
        gpg = {
          key = "${gpg_key}";
        };
      };
      "${gmail_meq_matt_astroid}" = {
        primary = false;
        address = "${gmail_matthieu_address}";
        userName = "${gmail_matthieu_address}";
        passwordCommand = "pass mail/${gmail_meq_matt_astroid} | head -n 1";
        imap = imap_gmail;
        smtp = smtp_gmail;
        realName = "Matthieu Méquignon";
        astroid = {
          enable = true;
        };
        neomutt = {
          enable = true;
        };
        notmuch.enable = true;
        mbsync = {
          enable = true;
          create = "maildir";
        };
        msmtp.enable = true;
        signature = {
          text = "${publicSignatureText}";
          showSignature = "append";
        };
      };
    };
  };

  services.mbsync = {
    enable = true;
    frequency = "*:0/30";
  };
}
