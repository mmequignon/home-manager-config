{ config, pkgs, ... }:

let
  fwzte_matthieu_address = import ./secrets/malignly.nix;
  gpg_signing_key = import ./secrets/indented.nix;
in {
  imports = [
    ./zsh.nix
    ./gpg.nix
    ./mail.nix
    ./packages.nix
  ];
  programs.home-manager.enable = true;

  home.username = "matthieu";
  home.homeDirectory = "/home/matthieu";

  nixpkgs.config.allowUnfree = true;

  xdg.userDirs.enable = true;

  gtk = {
    enable = true;
    theme = {
      name = "Adwaita-dark";
      package = pkgs.gnome3.gnome_themes_standard;
    };
  };

  xdg = {
    mimeApps = {
      enable = true;
      associations = {
        added = {
          "application/pdf" = ["org.pwmt.zathura.desktop" "org.gnome.Evince.desktop"];
          "application/xhtml+xml" = [ "firefox.desktop" ];
          "text/html" = [ "firefox.desktop" ];
          "text/xml" = [ "firefox.desktop" ];
          "x-scheme-handler/ftp" = [ "firefox.desktop" ];
          "x-scheme-handler/http" = [ "firefox.desktop" ];
          "x-scheme-handler/https" = [ "firefox.desktop" ];
          "application/vnd.mozilla.xul+xml" = [ "firefox.desktop" ];
          "audio/*" = [ "mpv.desktop" ];
          "video/*" = [ "mpv.desktop" ];
          "image/*" = [ "feh.desktop" ];
          "x-scheme-handler/magnet" = [ "transmission-gtk.desktop" ];
          "x-scheme-handler/mailto" = [ "astroid.desktop" ];
        };
        removed = {
          "application/pdf" = ["calibre-gui.desktop"];
          "application/xhtml+xml" = ["calibre-gui.desktop"];
          "text/html" = [ "calibre-gui.desktop" ];
          "text/xml" = [ "calibre-gui.desktop" ];
          "application/vnd.mozilla.xul+xml" = [ "calibre-gui.desktop" ];
        };
      };
      defaultApplications = {
        "application/pdf" = ["org.pwmt.zathura.desktop" "org.gnome.Evince.desktop"];
        "application/xhtml+xml" = [ "firefox.desktop" ];
        "text/html" = [ "firefox.desktop" ];
        "text/xml" = [ "firefox.desktop" ];
        "x-scheme-handler/ftp" = [ "firefox.desktop" ];
        "x-scheme-handler/http" = [ "firefox.desktop" ];
        "x-scheme-handler/https" = [ "firefox.desktop" ];
        "application/vnd.mozilla.xul+xml" = [ "firefox.desktop" ];
        "video/*" = [ "mpv.desktop" ];
        "image/*" = [ "feh.desktop" ];
        "x-scheme-handler/magnet" = [ "transmission-gtk.desktop" ];
        "x-scheme-handler/mailto" = [ "astroid.desktop" ];
      };
    };

    configFile = {
      "terminator/config".source = ./dotfiles/terminator.conf;
      "awesome".source = ./dotfiles/awesome;
      "mc".source = ./dotfiles/mc;
    };
  };

  home.sessionPath = [
    "~/bin/"
  ];

  home.file = {
    ".vim/pylintrc".source = ./dotfiles/pylintrc;
    ".vim/colors/selenized.vim".source = ./dotfiles/selenized.vim;
    ".config/nvim/colors/selenized.vim".source = ./dotfiles/selenized.vim;
    ".XCompose".source = ./dotfiles/xcompose;
    "Addresses.db".source = ./secrets/elzevirian.db;
    "bin".source = ./bin;
  };

  services = {

    # xmodmap = {
    #   enable = true;
    #   keysyms = {
    #     "keycode 133" = "3270_Left2";
    #   };
    # };
    sxhkd = {
      enable = true;
      keybindings = {
        "XF86Tools" = "rotate_screen";
      };
      extraPath = "${pkgs.python38Full}/bin:/home/matthieu/bin";
    };

    mpd = {
      enable = true;
      network.listenAddress = "0.0.0.0";
    };

    flameshot.enable = true;

    blueman-applet.enable = true;

    network-manager-applet.enable = true;

    redshift = {
      enable = true;
      tray = true;
      temperature.night = 4250;
      brightness.night = "0.7";
      latitude = "48.1";
      longitude = "-1.7";
    };

    unclutter.enable = true;

    # screenlocker TODO
    # xsuspender   TODO
  };

  programs = {
    kitty.enable = true;
    feh.enable = true;     # TODO configuration

    zathura.enable = true; # TODO configuration

    firefox.enable = true; # TODO configuration (profiles, userChrome.css)

    fzf.enable = true;     # TODO configuration

    mpv.enable = true;     # TODO configuration ?

    # TODO ssh configuration
    obs-studio = {
      enable = true;
      plugins = [
        pkgs.obs-v4l2sink
      ];
    };
    # TODO htop + config
    # TODO rofi

    git = {
      package = pkgs.gitAndTools.gitFull;
      enable = true;
      userEmail = "${fwzte_matthieu_address}";
      signing = {
        key = "${gpg_signing_key}";
        signByDefault = true;
      };
      aliases = {
        glog = "log --graph --decorate";
        ci = "commit";
        st = "status";
      };
      extraConfig = {
        core = {
          editor = "vim";
          pager = "";
          excludesfile = "~/.gitignore";
        };
        pull = {
          rebase = true;
        };
        user = {
          name = "Mmequignon";
        };
        hub = {
          protocol = "ssh";
        };
      };
    };

    vim = {
      enable = true;
      extraConfig = let
        filename = "~/Addresses.db";
      in ''
        ${builtins.readFile dotfiles/vimrc}
        if filereadable(expand("${filename}"))
          autocmd FileType mail :set complete+=k${filename}
          autocmd FileType mail :set iskeyword+=.-.
          autocmd FileType mail :set iskeyword+=@-@
          autocmd FileType mail :set iskeyword+=---
        endif
      '';
      plugins = with pkgs.vimPlugins; [
        ale
        nerdtree
        undotree
        vim-signify
        vim-airline
        vim-airline-themes
        vim-devicons
        fzf-vim
        ack-vim
        tagbar
        vim-colorschemes
        vim-nix
        vim-easymotion
        limelight-vim
        vim-fugitive
        fugitive-gitlab-vim
        nerdcommenter
        rust-vim
      ];
    };

    neovim = {
      enable = true;
      extraConfig = builtins.readFile dotfiles/vimrc;
      extraPython3Packages = (ps: with ps; [
        pylint
      ]);
      plugins = with pkgs.vimPlugins; [
        ale
        nerdtree
        undotree
        vim-signify
        vim-airline
        vim-airline-themes
        vim-devicons
        fzf-vim
        ack-vim
        tagbar
        vim-colorschemes
        vim-nix
        vim-easymotion
        nerdcommenter
      ];
    };

    tmux = {
      enable = true;
      extraConfig = builtins.readFile dotfiles/tmux.conf;
      plugins = with pkgs; [
        {
          plugin = tmuxPlugins.sidebar;
          extraConfig = "set -g @sidebar-tree-command 'tree -C'";
        }
      ];
    };

    newsboat = {
      enable = true;
      autoReload = false;
      urls = [
        { tags = ["fcul" "collegedefrance"]; url = "http://radiofrance-podcast.net/podcast09/rss_11921.xml"; }
        { tags = ["fcul" "culture" ]; url = "http://radiofrance-podcast.net/podcast09/rss_13954.xml"; }
        { tags = ["fcul" "culture" ]; url = "http://radiofrance-podcast.net/podcast09/rss_17360.xml"; }
        { tags = ["fcul" "environnement" ]; url = "http://radiofrance-podcast.net/podcast09/rss_16410.xml"; }
        { tags = ["fcul" "international" "culture"]; url = "http://radiofrance-podcast.net/podcast09/rss_11701.xml"; }
        { tags = ["fcul" "international" ]; url = "http://radiofrance-podcast.net/podcast09/rss_13305.xml"; }
        { tags = ["fcul" "science" ]; url = "http://radiofrance-podcast.net/podcast09/rss_14312.xml"; }
        { tags = ["fcul" "science" ]; url = "http://radiofrance-podcast.net/podcast09/rss_14312.xml"; }
        { tags = ["fcul" "science"]; url = "http://radiofrance-podcast.net/podcast09/rss_13957.xml"; }
        { tags = ["news" "fcul" "actu" ]; url = "http://radiofrance-podcast.net/podcast09/rss_13212.xml"; }
        { tags = ["news" "fcul" "une" "culture" "international" ]; url = "http://radiofrance-podcast.net/podcast09/rss_10075.xml"; }
        { tags = ["news" "lemonde" "planete" "agriculture"]; url = "https://www.lemonde.fr/agriculture/rss_full.xml";}
        { tags = ["news" "lemonde" "planete" "climat"]; url = "https://www.lemonde.fr/climat/rss_full.xml";}
        { tags = ["news" "lemonde" "planete" "environnement"]; url = "https://www.lemonde.fr/environnement/rss_full.xml";}
        { tags = ["news" "lemonde" "une" "culture"]; url = "https://www.lemonde.fr/culture/rss_full.xml";}
        { tags = ["news" "lemonde" "une" "international"]; url = "https://www.lemonde.fr/international/rss_full.xml";}
        { tags = ["news" "lemonde" "une" "planete"]; url = "https://www.lemonde.fr/planete/rss_full.xml";}
        { tags = ["news" "lemonde" "une" "politique"]; url = "https://www.lemonde.fr/politique/rss_full.xml";}
        { tags = ["news" "lemonde" "une" "science"]; url = "https://www.lemonde.fr/culture/rss_full.xml";}
        { tags = ["news" "lemonde" "une"]; url = "https://www.lemonde.fr/rss/une.xml";}
        { tags = ["news" "libe" "une" "culture"]; url = "http://rss.liberation.fr/rss/54/";}
        { tags = ["news" "libe" "une" "planete"]; url = "http://rss.liberation.fr/rss/10/";}
        { tags = ["news" "libe" "une" "politique"]; url = "http://rss.liberation.fr/rss/100666/";}
        { tags = ["news" "libe" "une"]; url = "http://rss.liberation.fr/rss/9/";}
        { tags = ["science"]; url = "https://www.cafe-sciences.org/feed/"; }
        { tags = ["tech"]; url = "http://feeds.podtrac.com/IOJSwQcdEBcg"; }
        { tags = ["tech"]; url = "http://lehollandaisvolant.net/rss.php"; }
        { tags = ["tech"]; url = "http://linuxfr.org/journaux.atom"; }
        { tags = ["tech"]; url = "http://linuxfr.org/news.atom"; }
        { tags = ["tech"]; url = "http://sametmax.com/feed"; }
        { tags = ["tech"]; url = "http://sebsauvage.net/links/?do=rss"; }
      ];
      extraConfig = builtins.readFile dotfiles/newsboat.conf;
    };

    ncmpcpp = {
      enable = true;
      bindings = [
        {key = "j"; command="scroll_down";}
        {key = "k"; command="scroll_up";}
        {key = "h"; command="previous_column";}
        {key = "l"; command="next_column";}
        {key = "L"; command="next_screen";}
        {key = "H"; command="previous_screen";}
        {key = "g"; command="move_home";}
        {key = "G"; command="move_end";}
      ];
      mpdMusicDir = "/home/matthieu/music";
      settings = {
        mpd_crossfade_time = "5";
        discard_colors_if_item_is_selected = "yes";
        header_window_color = "black";
        volume_color = "cyan";
        state_line_color = "green";
        state_flags_color = "red";
        main_window_color = "default";
        color1 = "default";
        color2 = "black";
        current_item_inactive_column_prefix = "$(red)$r";
        current_item_inactive_column_suffix = "$/r$(end)";
        current_item_prefix = "$(red)$r";
        current_item_suffix = "$/r$(end)";
       progressbar_color = "green";
        visualizer_color = "cyan";
      };

    };

    autorandr = {
      enable = true;
      profiles = {
        "mobile" = {
          fingerprint = {
            eDP-1 = "00ffffffffffff0030e437040000000000170104951c107802bf059a59558e261d505400000001010101010101010101010101010101163680ba70380f4030203500149c1000001a000000000000000000000000000000000000000000fe004c4720446973706c61790a2020000000fe004c503132355746322d5350423200dd";
          };
          config = {
            eDP-1 = {
              enable = true;
              primary = true;
              position = "0x0";
              mode = "1920x1080";
            };
          };
          hooks.postswitch = ''
            xsetwacom --set 11 Rotate 0
            xinput --set-prop 9 175 1 0 0 0 1 0 0 0 1
          '';
        };
      };
    };
  };

  manual.manpages.enable = true;

  home.stateVersion = "20.09";
}
