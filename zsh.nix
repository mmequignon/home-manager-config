{ config, pkgs, ...}:

{
  home.file.".zsh/completions/_hub".source = ./dotfiles/_hub;

  programs = {
    zsh = {
      enable = true;
      enableAutosuggestions = true;
      enableCompletion = true;
      shellAliases = {
        cpr = "rsync -a --info = progress2";
        passgen = "pwgen -sy 15 1";
        ls = "ls -sh --color=auto ";
        ll = "ls -lh";
        lla = "ls -lah";
        llas = "ls -laSh";
        grep = "grep --color";
        tmux = "tmux -2";
        git = "hub";
      };
      sessionVariables = {
        PATH = "$PATH:/home/matthieu/bin";
        EDITOR = "vim";
        TERM = "xterm-256color";
      };
      initExtra = ''
        autoload -U compinit && compinit
      '';
      oh-my-zsh = {
        enable = true;
      };
    };

    starship = {
      enable = true;
      enableZshIntegration = true;
      settings = {
        add_newline = false;
        line_break.disabled = true;
        username = {
          show_always = true;
          format= "[$user]($style)@";
        };
        directory = {
          style = "bold blue";
          truncate_to_repo = false;
        };
        hostname = {
          ssh_only = false;
          format = "[$hostname]($style) in ";
        };
        python.disabled = true;
        status.disabled = true;
        character.format = " ";
        cmd_duration.disabled = true;
        git_status.disabled = true;
        prompt_orders = [ "username" "hostname" "directory" "git_branch" "git_state" ];
      };
    };
  };
}
